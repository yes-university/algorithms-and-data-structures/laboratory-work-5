import yargs from "yargs";
import { faker } from "@faker-js/faker/locale/uk";
import bootstrap from "../utils";

interface Factory {
    name: string;
    middleAge: number;
    specialty: string;
    averageSalary: number;
}

const factories: Factory[] = [];

async function performInput(text: string[]): Promise<any> {
    const performedText = text[0] === "notebook" ? text.slice(1) : text;
    return yargs(!performedText.length ? ["--help"] : performedText)
        .scriptName("factory")
        .version("default")
        .usage("$0 <cmd> [args]")
        .command("clear", "Clear console", {}, () => {
            console.clear();
        })
        .command(
            "add",
            "Add new factory",
            {
                name: {
                    alias: "n",
                    description:
                        "Name of the factory. Could be unique in notebook",
                    required: true,
                    type: "string",
                },
                "middle-age": {
                    alias: "m",
                    description: "Middle age of the factory",
                    required: true,
                    type: "number",
                },
                specialty: {
                    alias: "s",
                    description: "Specialty of the factory",
                    required: true,
                    type: "string",
                },
                "average-salary": {
                    alias: "a",
                    description: "Average salary of the factory",
                    required: true,
                    type: "number",
                },
            },
            (args) => {
                factories.push({
                    name: args.name,
                    middleAge: args["middle-age"],
                    specialty: args.specialty,
                    averageSalary: args["average-salary"],
                });
                console.log("Factory added");
                console.log("Factories:", factories);
            }
        )
        .command(
            "remove",
            "Remove factory by index",
            {
                index: {
                    alias: "i",
                    description: "Index of factory to remove",
                    required: true,
                    type: "number",
                },
            },
            (args) => {
                const removed = factories.splice(args.index, 1);
                console.log("Factory removed:", removed);
                console.log("Factories:", factories);
            }
        )
        .command(
            "reveal",
            "Reveal factories",
            {
                index: {
                    alias: "i",
                    description: "Index of factory to reveal",
                    required: false,
                    type: "number",
                    default: -1,
                },
            },
            (args) => {
                if (args.index === -1) {
                    console.log("Factories:", factories);
                } else {
                    console.log("Factory:", factories[args.index]);
                }
            }
        )
        .command(
            "fill",
            "Quickly fill factories with random data (faker)",
            {
                amount: {
                    alias: "c",
                    description: "How many factories to add",
                    default: 10,
                    type: "number",
                },
            },
            (args) => {
                for (let i = 0; i < args.amount; i++) {
                    factories.push({
                        name: faker.company.name(),
                        middleAge: faker.number.int(100),
                        specialty: faker.person.jobArea(),
                        averageSalary: faker.number.int(10000),
                    });
                }
                console.log("Factories filled");
                console.log("Factories:", factories);
            }
        )
        .command(
            "show",
            "show factories, where average salary is more than average across all factories",
            {},
            () => {
                const averageSalary =
                    factories.reduce(
                        (acc, factory) => acc + factory.averageSalary,
                        0
                    ) / factories.length;
                console.log(
                    "Average salary across all factories:",
                    averageSalary
                );
                const filteredFactories = factories.filter(
                    (factory) => factory.averageSalary > averageSalary
                );
                console.log("Filtered factories:", filteredFactories);
            }
        )
        .help()
        .exitProcess(false)
        .fail((msg, err, y) => {
            console.error(msg);
            if (err) throw err;
            y.showHelp();
        })
        .showHelpOnFail(true)
        .parse();
}

bootstrap(performInput, "factory");
